FROM maven:3.3-jdk-8 as builder

WORKDIR /app

ADD . /app

RUN mvn clean package

# Runner

FROM openjdk:8-jdk-alpine

COPY --from=builder /app/target target/

ENTRYPOINT ["java", "-jar", "target/ip-config-0.0.1-SNAPSHOT.jar"]
